package theplague.classs

import theplague.interfaces.Iconizable

abstract class Item: Iconizable {
    var timesLeft: Int = 5
   open fun use(){
       timesLeft--
   }
}