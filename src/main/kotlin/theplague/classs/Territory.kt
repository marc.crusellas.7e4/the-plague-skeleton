package theplague.classs

import theplague.classs.weapon.Weapon
import theplague.interfaces.ITerritory
import theplague.interfaces.Iconizable


class Territory(x: Int, y: Int) : ITerritory {

    var hasPlayer: Boolean = false

    var plagueSize = 0

    var item: Item? = null

    fun exterminate(weapon: Weapon) {
        // todo
    }

    override fun iconList(): List<Iconizable> {
        return if (item != null)
            List(plagueSize) {
                item!!
            }
        else {
            listOf()
        }
    }


}