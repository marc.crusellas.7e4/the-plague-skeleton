package theplague.logic

import theplague.classs.Territory
import theplague.interfaces.*


class World(override val width: Int, override val height: Int) : IWorld {


    override val territories: List<List<Territory>> = List(width) { x ->
        List(height) { y ->
            Territory(x, y)
        }
    }
    override val player: IPlayer
        get() = TODO("Not yet implemented")

    override fun nextTurn() {
        TODO("Not yet implemented")
    }

    override fun gameFinished() = false

    override fun canMoveTo(position: Position): Boolean {
        TODO("Not yet implemented")
    }

    override fun moveTo(position: Position) {
        TODO("Not yet implemented")
    }

    override fun exterminate() {
        //  TODO("Not yet implemented")
    }

    override fun takeableItem(): Iconizable? {
        TODO("Not yet implemented")
    }

    override fun takeItem() {
        TODO("Not yet implemented")
    }

    /**
     * extra
     */
    fun randomPosition(): Position {
        return Position(List(width) { it }.random() + 1, List(height) { it }.random() + 1)
    }

    private fun generateNewColones() {
        // TODO
    }
    // fun place(colonization: Colonization) {}

    private fun generateNewItems() {
        // todo
    }

    private fun reproduce() {
        // todo
    }

    private fun expand() {
        // TODO
    }


}