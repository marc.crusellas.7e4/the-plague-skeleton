package theplague.ui.gui

import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import kotlinx.coroutines.launch
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import theplague.interfaces.*
import kotlinx.coroutines.channels.Channel

@ExperimentalMaterialApi
@Composable
@Preview
fun MainScreen(worldParam: IWorld) {
    val scope = rememberCoroutineScope()
    var world by remember{mutableStateOf<IWorld>(worldParam)}
    var worldData by remember{mutableStateOf<ImmutableWorldData>(worldParam.toImmutableWorld())}
    val snackbarHostState = remember { SnackbarHostState() }
    var pickItemDialog by remember { mutableStateOf<Iconizable?>(null)}
    val pickItemChannel = Channel<Boolean>()

//    val roboto = FontFamily(
//        androidx.compose.ui.text.platform.Font(
//            resource = "Roboto-Regular.ttf",
//            weight = FontWeight.Normal,
//            style = FontStyle.Normal
//        )
//    )
//    val typography = Typography(defaultFontFamily = roboto)


    MaterialTheme() {
        if (pickItemDialog!=null) {
            PickItemDialog(pickItemDialog!!){
                pickItemDialog = null
                scope.launch { pickItemChannel.send(it) }
            }
        }
        if(worldData.gameFinished){
            EndGameScreen(worldData.player)
        } else {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                UserDataDisplay(worldData.player)


                worldData.territories.forEachIndexed { y, row ->
                    Row() {
                        row.forEachIndexed { x, cell ->
                            TerritoryDisplay(cell) {
                                scope.launch {
                                    val position = Position(x, y)
                                    if (!world.canMoveTo(position)) {
                                        snackbarHostState.showSnackbar("You can't go to this position with the current vehicle")
                                        return@launch
                                    }

                                    world.moveTo(position)
                                    val item = world.takeableItem()
                                    if (item != null) {
                                        pickItemDialog = item
                                        val take = pickItemChannel.receive()
                                        if (take)
                                            world.takeItem()
                                    }
                                    world.exterminate()
                                    world.nextTurn()
                                    worldData = world.toImmutableWorld()
                                }
                            }
                        }
                    }
                }
            }

        }
        SnackbarHost(hostState = snackbarHostState)
    }
}